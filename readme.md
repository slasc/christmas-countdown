# Christmas Countdown

![Image](./scrshots/countdown-ticker.jpg)

A simple project for showing the number of days until next Christmas on an LED matrix.

*Disclaimer*: This is probably not my most organized code project. It was started as a simple thing to amuse my kids. 

## Setup

You will need to configure the pin settings for the LED Matrix.
You will also need to create a secrets.h header file to contain your wifi ssid and psk.

Example:<br/>
#define wifi_ssid "`<your wifi ssid>`"<br/>
#define wifi_psk "`<your wifi psk>`"<br/>

## Font Definitions

![Image](./scrshots/font-sample.png)

You can use the following site to help define your font(s)<br/>

- [MD_MAX72XX - FONT EDITOR](https://pjrp.github.io/MDParolaFontEditor)
- [Source for above FONT EDITOR](https://github.com/pjrp/MD_MAX72XX-MD_Parola-Font-Editor)


## Hardware

This is a project designed to run on an ESP32 with an LED 8x32 matrix (MAX7219).

### ESP32

- [Pinout](https://esphome.io/devices/nodemcu_esp32.html)

- [AZDelivery ESP32 NodeMCU Module WLAN WiFi Dev Kit C Development Board with CP2102 - Amazon Listing](https://www.amazon.de/-/en/gp/product/B071P98VTG/ref=ppx_yo_dt_b_asin_title_o00_s00?ie=UTF8&psc=1)

### LED 8x32 (MAX7219) Matrix

- [kwmobile 8 x 32 LED Matrix Module for Raspberry Pi and Arduino - Amazon Listing](https://www.amazon.de/-/en/gp/product/B06XJ9ZX17/ref=ppx_yo_dt_b_asin_title_o07_s00?ie=UTF8&psc=1)

## Software Libraries

For the ESP32 board:

This should include the necessary WiFi libraries

Links to add the appropriate ESP32 boards libraries:<br/>
`https://dl.espressif.com/dl/package_esp32_index.json, http://arduino.esp8266.com/stable/package_esp8266com_index.json`

![add boards](./scrshots/Preferences-AddBoards.png)

Other Libraries:

### For the LED Matrix

#### MD_Parola
- [MD_Parola source code on GitHub](https://github.com/MajicDesigns/MD_Parola)
- [MD_Parola documentation](https://majicdesigns.github.io/MD_Parola/)

#### MD_MAX72XX
- [MD_MAX72XX source code on GitHub](https://github.com/MajicDesigns/MD_MAX72xx)
- [MD_MAX72XX documentation](https://majicdesigns.github.io/MD_MAX72XX/page_connect.html)

### NTP Client (NTPClient)

- [Github Repository - NTPClient](https://github.com/arduino-libraries/NTPClient)

### Time Library

- [Time Library](https://playground.arduino.cc/Code/Time/)
- [Time Library on Github](https://github.com/PaulStoffregen/Time)
