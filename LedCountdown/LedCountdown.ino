// Include the required matrix libraries
#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

//### Wifi Libraries
#include <WiFiClientSecure.h>

//### Ntp Library
#include <NTPClient.h>

//### Time Library
#include <TimeLib.h>

//### Speed Settings
//### Loop Delay in ms
#define LOOPDELAY 500
#define GLYPHINTERVAL 30000

//### From Documentation - https://www.makerguides.com/max7219-led-dot-matrix-display-arduino-tutorial/

//### hardware type and pin defines for the led matrix
#define HARDWARE_TYPE MD_MAX72XX::ICSTATION_HW
#define MAX_DEVICES 4
#define CS_PIN 14
#define DATA_PIN 27
#define CLK_PIN 26

//MD_Parola myDisplay = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES); // hardware spi
MD_Parola myDisplay = MD_Parola(HARDWARE_TYPE, DATA_PIN, CLK_PIN,  CS_PIN, MAX_DEVICES); //software spi

//### Christmas Font
#include "ChristmasFont.h"

//### Standard Glyphs
const char Snowflake = (char)126;
const char Snowflake2 = (char)148;
const char Tree = (char)127;
const char Reindeer = (char)129;
const char Present = (char)130;
const char Presents = (char)131;
const char Sleigh = (char)128;
const char CandyCane = (char)132;
const char Wreath = (char)133;
const char Manger = (char)134;
const char Stocking = (char)140;
const char Star = (char)135;


//### Special Glyphs
const char SocialReindeer = (char)154;
const char FlyingSleigh1 = (char)149;
const char FlyingSleigh2 = (char)150;
const char FlyingSleigh3 = (char)151;


//### Merry Christmas
const char Salutation = (char)155;

//### The array of active glyphs.
const char charGlyphArray[] = {Tree, Reindeer, Present, Presents, Sleigh, Snowflake, Snowflake2, CandyCane, Wreath, Manger, Stocking, Tree, Star};
  
char prefix = Tree;
char suffix = Reindeer;

/**
 * @brief Grabs a random glyph
 */
char randomCharSelect(char _prev = (char)0)
{  
 
  //### Calculate the size of the array
  auto arraySize = sizeof(charGlyphArray) / sizeof(char);

  //### Grab a "random" glyph
  auto index = random(0,arraySize);

  //### Make sure we are grabbing a different one from the previous value.
  while (_prev != (char)0 && _prev == charGlyphArray[index])
    index = random(0,arraySize);

  //### Return the new glyph.
  return charGlyphArray[index];
}

//### This is for randomizing the display for the Christmas day
int christmasGlyphIndex = 0;

//### Wifi settings
#include "secrets.h"

const char *ssid     = wifi_ssid; // defined in secrets.h
const char *password = wifi_psk;  // defined in secrets.h

//### NTP settings

WiFiUDP ntpUDP;

// By default 'pool.ntp.org' is used with 60 seconds update interval
constexpr int timeOffset =  1 * 60 * 60; //s
NTPClient timeClient(ntpUDP, timeOffset);

//### Time stuff
time_t lastUpdated = now();
time_t lastGlyphUpdated = millis();

String getFullTime()
{
  auto time = now();
  auto result = String(hour(time)) + ":" + String(minute(time)) + ":" + String(second(time)) + " -- " + String(month(time)) + "/" + String(day(time)) + "/" + String(year(time));
  return result;
}

/**
 * @brief Initializes the serial connection (important for debugging)
 * 
 */
void initSerial()
{
  Serial.begin(115200);
  Serial.println("Serial Connection Initialized.");
}

/**
 * @brief Initializes the LED Matrix
 * 
 */
void initMatrix()
{
  Serial.println("Starting initMatrix()");

  // Intialize the object:
  myDisplay.begin();
  
  // Setup zones
  myDisplay.setZone(0, 0, 3);
  
  // Set the intensity (brightness) of the display (0-15):
  myDisplay.setIntensity(3);
  
  // Clear the display:
  myDisplay.displayClear();
  
  // Set Display Text Alignment
  myDisplay.setTextAlignment(PA_LEFT);

  // Set the Font
  myDisplay.setFont(ChristmasArtFont);
}

/**
 * @brief Initializes the WiFi
 * 
 */
void initWiFi()
{
  char search = (char)146;
  char wifi = (char)147;
  Serial.println("Starting iniWiFi()");
  displayText(String(search) + "Srch..");

  WiFi.begin(ssid, password);  

  int Count = 0;
  while ( WiFi.status() != WL_CONNECTED ) 
  {
    Count++;
    delay ( 500 );
    Serial.print ( "." );
    if (Count == 30)
    {
      Count = 0;
      WiFi.begin(ssid, password);
    }
  }
  displayText(String(wifi) + "Conn!");  
  Serial.printf("Connected to %s \r\n", ssid);
  delay(1000);
}

/**
 * @brief Initializes the NTP Client
 * 
 */
void initNtp()
{
  Serial.println("Starting initNtp()");

  timeClient.begin();

  updateTime();
}

void updateTime()
{
  timeClient.update();

  Serial.printf("Ntp updating time: %s\r\n", timeClient.getFormattedTime());
 
  setTime(timeClient.getEpochTime());
  
  Serial.printf("Current time is : %s\r\n", getFullTime().c_str());
}

/**
 * @ Main Setup Function for Arduino
 * 
 */
void setup() 
{
  initSerial();   
  
  Serial.println("Starting setup()");

  randomSeed(analogRead(0));

  //### Initialize the LED Matrix
  initMatrix();

  //### Initialize WiFi
  initWiFi();

  //### Initialize Ntp
  initNtp();

  // Clear the display:
  myDisplay.displayClear();  
}

/**
 * @brief Calculates the time until Christmas
 * 
 */
void calculateTimeTilChristmas()
{
  tmElements_t christmas_struct;
  
  breakTime(now(), christmas_struct);
  
  if (christmas_struct.Day > 25 && christmas_struct.Month == 25)
    christmas_struct.Year++;
  
  christmas_struct.Day = 25;
  christmas_struct.Month = 12;

  christmas_struct.Hour = 0;
  christmas_struct.Minute = 0;
  christmas_struct.Second = 0;

  
  auto Christmas = makeTime(christmas_struct);
  
  auto daysTilChristmas = elapsedDays(Christmas) - elapsedDays(now());
  
  auto hoursTilChristmas = 24 - ((numberOfHours(now()) % 24) + 1);
  auto minTilChristmas = 60 - (numberOfMinutes(now()) % 60);
  auto secTilChristmas = 60 - (numberOfSeconds(now()) % 60);

  //Serial.printf("number of hours for Christmas: %d\r\n",numberOfHours(Christmas));
  //Serial.printf("number of hours for now: %d\r\n",numberOfHours(now()));

  if (daysTilChristmas == 0)
  {
    displayText(String(Salutation));
    return;
  }
  else
    if (daysTilChristmas == 2)
    {
      prefix = Sleigh;
      //suffix = SocialReindeer;
    }
    else
      if (daysTilChristmas == 1)
      {
        //### If Christmas Eve, give an hours:minutes:seconds countdown instead.

        //### The last hour should be counter only.
        if (hoursTilChristmas < 1)
          christmasGlyphIndex = 3;

        char hours[3];
        sprintf(hours, "%01d\0", hoursTilChristmas);
        char minutes[3];
        sprintf(minutes, "%02d\0", minTilChristmas);
        char seconds[3];
        sprintf(seconds, "%02d\0", secTilChristmas);

        //### Rotate between flying sleigh and countdown clock
        switch (christmasGlyphIndex)
        {
          case 0 : displayText(String(hours) + "h" + String(FlyingSleigh1)); break;
          case 1 : displayText(String(hours) + "h" + String(FlyingSleigh2)); break;
          case 2 : displayText(String(hours) + "h" + String(FlyingSleigh3)); break;
          case 3 : 
          default : displayText(String(hours) + ":" + String(minutes) + ":" + String(seconds) + String(suffix));       
        }
        return;
      }    
  displayText(String(prefix) + String(" ") + String(daysTilChristmas) + String(" ") + String(suffix));
}

/**
 * @brief Displays a line of text on the LED Matrix
 * 
 */
void displayText(String text)
{
  myDisplay.print(text);
}


/**
 * @brief Main loop function for Arduino
 * 
 */
void loop() {
  //Serial.println("Starting loop()");

  //### Update Ntp Client
  if ((numberOfMinutes(now()) - numberOfMinutes(lastUpdated)) > 5)
  {
    updateTime();
    
    lastUpdated = now();
  }

  //### Update prefix and suffix glyphs
  if (millis() > GLYPHINTERVAL && ((millis() - GLYPHINTERVAL) > lastGlyphUpdated || millis() < lastGlyphUpdated))
  {
    Serial.printf("Updating glyph selection at %s\r\n", getFullTime().c_str());

    lastGlyphUpdated = millis();
    
    //### Also update the prefix and suffix glyphs.
    prefix = randomCharSelect(prefix);
    delay(10);
    suffix = randomCharSelect(suffix);

    //### Set the christmasGlyphIndex to a random 0 - 3 value (only truly necessary for Christmas Day)
    christmasGlyphIndex = random(0,5);
  }
  
  
  //### Calculate time until Christmas
  calculateTimeTilChristmas();

  //### Delay
  delay(LOOPDELAY);
}
